﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using todo_app.Controllers;
using todo_app.DAO;
using Xunit;
using FluentAssertions;
using System.Collections.Generic;
using todo_app.Models;
using System.Linq;
using todo_app.Dtos;

namespace todo_app.Tests
{
    public class TodosControllerTests : TodosXUnit
    {
        [Fact]
        public void GetAllTodosShouldReturnOK()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetAllTodos();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            actionResult.Should().BeOfType<ActionResult<IEnumerable<Todo>>>();
        }

        [Fact]
        public void GetTodoByIdShouldReturnOK()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            int todoId = dbContext.Todos.Select(x => x.Id).FirstOrDefault();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetTodoById(todoId);
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            actionResult.Should().BeOfType<ActionResult<Todo>>();
        }

        [Fact]
        public void GetTodoByIdShouldReturnNotFound()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            int todoId = dbContext.Todos.Select(x => x.Id).LastOrDefault();
            todoId++;

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetTodoById(todoId);
            actionResult.Result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public void GetNumberActiveTodosShouldReturnOK()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            int numberActiveTodos = dbContext.Todos.Where(x => x.Active == true).Count();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetNumberActiveTodos();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            // add actual number check  - result.value
        }

        [Fact]
        public void GetNumberActiveTodosShouldReturnNotFound()
        {
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetNumberActiveTodos();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public void GetNumberCompletedTodosShouldReturnOK()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            int numberCompletedTodos = dbContext.Todos.Where(x => x.Active == false).Count();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetNumberCompletedTodos();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            // add actual number check  - result.value
        }

        [Fact]
        public void GetNumberCompeltedTodosShouldReturnNotFound()
        {
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.GetNumberCompletedTodos();
            actionResult.Result.Should().BeOfType<OkObjectResult>();
        }

        [Fact]
        public void CreateTodoShouldReturnTodo()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.CreateTodo(new TodoCreateDto()
                                                            {
                                                                Title = "test3",
                                                                Active = true
                                                            });

            actionResult.Result.Should().BeOfType<CreatedAtRouteResult>();
            actionResult.Should().BeOfType<ActionResult<TodoReadDto>>();
        }

        [Fact]
        public void UpdateTodoShouldReturnNoContentResult()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            var todo = dbContext.Todos.FirstOrDefault();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.UpdateTodo(todo.Id, new TodoUpdateDto()
                                                                    {
                                                                        Title = "test3",
                                                                        Active = false
                                                                    });
            actionResult.Should().BeOfType<NoContentResult>();

            var updatedTodoTitle = dbContext.Todos.Where(x => x.Id == todo.Id).Select(x => x.Title);
            var updatedTodoActive = dbContext.Todos.Where(x => x.Id == todo.Id).Select(x => x.Active);
            updatedTodoTitle.Should().Equal("test3");
            updatedTodoActive.Should().Equal(false);
        }

        [Fact]
        public void DeleteTodoShouldReturnNoContentResult()
        {
            var dbContext = ServiceProvider.GetRequiredService<TodosContext>();
            var todosRepository = ServiceProvider.GetRequiredService<ITodos>();
            var mapper = ServiceProvider.GetRequiredService<IMapper>();

            new TodoAddData().CreateTestData(dbContext);
            var todoId = dbContext.Todos.Select(x => x.Id).FirstOrDefault();

            var todosController = new TodosController(todosRepository, mapper);

            var actionResult = todosController.DeleteTodo(todoId);
            actionResult.Should().BeOfType<NoContentResult>();

            var deletedTodoExist = dbContext.Todos.Where(x => x.Id == todoId).Count();
            deletedTodoExist.Should().Be(0);
        }
    }
}
