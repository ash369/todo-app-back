﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using todo_app.DAO;

namespace todo_app.Tests
{
    public class TodosXUnit
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IConfigurationRoot Configuration { get; set; }

        public TodosXUnit()
        {
            var services = new ServiceCollection();
            services.AddEntityFrameworkInMemoryDatabase();

            services.AddDbContext<TodosContext>(opt => opt.UseInMemoryDatabase("Todos").UseInternalServiceProvider(ServiceProvider));
            services.AddScoped<ITodos, Todos>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            ServiceProvider = services.BuildServiceProvider();
        }
    }
}
