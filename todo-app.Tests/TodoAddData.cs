﻿using System.Collections.Generic;
using todo_app.DAO;
using todo_app.Models;

namespace todo_app.Tests
{
    class TodoAddData
    {
        public void CreateTestData(TodosContext dbContext)
        {
            var todos = new List<Todo>
            {
                new Todo()
                {
                    Id = 1,
                    Title = "test1",
                    Active = true
                },
                new Todo()
                {
                    Id = 2,
                    Title = "test2",
                    Active = false
                }
            };

            dbContext.Todos.AddRange(todos);
            dbContext.SaveChanges();
        }
            
    }
}
