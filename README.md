# Todo Application Backend

This was built as part of a Developer Bootcamp Project.  

This application is the back end WebAPI and will serve requests for Todo items like adding a todo , updating a todo etc. 
Part of the project was to run the application in a cloud provider of our choice inside docker containers, as well as have pipelines that tests, builds and pushes the images to the cloud provider.

A full list of requirements for the project can be found here: [awesome-bootcamp-group/todo-base](https://gitlab.com/awesome-bootcamp-group/todo-base)

## Application Details
The application was built using .NET Core 3.1 together with the following:
- EntityFrameworkCore
- SQL Server Database (running in Azure)
- (Testing) XUnit
- (Testing) FluentAssertions
- (Testing) EntityFrameworkCore InMemory Database