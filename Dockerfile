FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY todo-app/todo-app.csproj ./
RUN dotnet restore

COPY . .
#RUN sed -i "s/AZURE_DATABASE_USER/$AZURE_DATABASE_USER/g" appsettings.json
#RUN sed -i "s/AZURE_DATABASE_PASSWORD/$AZURE_DATABASE_PASSWORD/g" appsettings.json
RUN dotnet publish /app/todo-app/ -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "todo-app.dll"]