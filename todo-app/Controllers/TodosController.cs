﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using todo_app.DAO;
using todo_app.Dtos;
using todo_app.Models;

namespace todo_app.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly ITodos _repository;
        private readonly IMapper _mapper;

        public TodosController(ITodos repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET api/todos
        [HttpGet]
        public ActionResult<IEnumerable<Todo>> GetAllTodos()
        {
            var todoItems = _repository.GetAllTodos();
            return Ok(_mapper.Map<IEnumerable<Todo>>(todoItems));
        }

        // GET api/todos/{id}
        [HttpGet("{id}", Name = "GetTodoById")]
        public ActionResult<Todo> GetTodoById(int Id)
        {
            var todoItem = _repository.GetTodoById(Id);

            if (todoItem != null)
            {
                return Ok(_mapper.Map<TodoReadDto>(todoItem));
            }

            return NotFound();
        }

        // GET api/todos/number-active-todos
        [HttpGet("number-active-todos")]
        public ActionResult<int> GetNumberActiveTodos()
        {
            var numberActiveTodos = _repository.GetNumberActiveTodos();

            if (numberActiveTodos != null)
            {
                return Ok(numberActiveTodos);
            }

            return NoContent();
        }

        // GET api/todos/number-completed-todos
        [HttpGet("number-completed-todos")]
        public ActionResult<int> GetNumberCompletedTodos()
        {
            var numberCompeletedTodos = _repository.GetNumberCompletedTodos();

            if (numberCompeletedTodos != null)
            {
                return Ok(numberCompeletedTodos);
            }

            return NoContent();
        }

        // POST api/todos
        [HttpPost]
        public ActionResult<TodoReadDto> CreateTodo(TodoCreateDto todoCreateDto)
        {
            var todoModel = _mapper.Map<Todo>(todoCreateDto);
            _repository.CreateTodo(todoModel);
            _repository.SaveChanges();
            var todoReadDto = _mapper.Map<TodoReadDto>(todoModel);
            return CreatedAtRoute(nameof(GetTodoById), new { Id = todoReadDto.Id }, todoReadDto);
        }

        // PUT api/todos/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateTodo(int Id, TodoUpdateDto todoUpdateDto)
        {
            var todoModelFromRepo = _repository.GetTodoById(Id);

            if (todoModelFromRepo == null)
            {
                return NotFound();
            }

            _mapper.Map(todoUpdateDto, todoModelFromRepo);
            _repository.UpdateTodo(todoModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }

        // PUT api/todos
        [HttpPut]
        public ActionResult UpdateTodos([FromBody] List<TodoReadDto> todoList)
        {
            foreach (TodoReadDto todoReadDto in todoList)
            {
                var todoModelFromRepo = _repository.GetTodoById(todoReadDto.Id);

                _mapper.Map(todoReadDto, todoModelFromRepo);
                _repository.UpdateTodo(todoModelFromRepo);                
            }

            _repository.SaveChanges();
            return NoContent();
        }

        // DELETE api/todos/{id}
        [HttpDelete("{Id}")]
        public ActionResult DeleteTodo(int Id)
        {
            var todoModelFromRepo = _repository.GetTodoById(Id);

            if (todoModelFromRepo == null)
            {
                return NotFound();
            }

            _repository.DeleteTodo(todoModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }

        // DELETE api/todos
        [HttpDelete]
        public ActionResult DeleteTodos([FromBody] List<Todo> todoList)
        {
            if (todoList == null)
            {
                return NotFound();
            }

            _repository.DeleteTodos(todoList);
            _repository.SaveChanges();
            return NoContent();
        }
    }
}
