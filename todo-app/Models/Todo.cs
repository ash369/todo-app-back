﻿using System.ComponentModel.DataAnnotations;

namespace todo_app.Models
{
    public class Todo
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(250)]
        public string Title { get; set; }
        
        [Required]
        public bool Active { get; set; }
    }
}
