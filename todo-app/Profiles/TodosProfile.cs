﻿using AutoMapper;
using todo_app.Dtos;
using todo_app.Models;

namespace todo_app.Profiles
{
    public class TodosProfile : Profile
    {
        public TodosProfile()
        {
            CreateMap<Todo, TodoReadDto>();
            CreateMap<TodoCreateDto, Todo>();
            CreateMap<TodoUpdateDto, Todo>();
            CreateMap<TodoReadDto, Todo>();
        }
    }
}
