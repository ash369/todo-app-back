﻿using Microsoft.EntityFrameworkCore;
using todo_app.Models;

namespace todo_app.DAO
{
    public class TodosContext : DbContext
    {
        public TodosContext(DbContextOptions<TodosContext> opt) : base(opt)
        {

        }

        public DbSet<Todo> Todos { get; set; }
    }
}
