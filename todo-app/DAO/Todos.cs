﻿using System;
using System.Collections.Generic;
using System.Linq;
using todo_app.Models;

namespace todo_app.DAO
{
    public class Todos : ITodos
    {
        private readonly TodosContext _context;

        public Todos(TodosContext context)
        {
            _context = context;
        }

        public void CreateTodo(Todo todo)
        {
            if (todo == null)
            {
                throw new ArgumentNullException(nameof(todo));
            }

            _context.Todos.Add(todo);
        }

        public void DeleteTodo(Todo todo)
        {
            if (todo == null)
            {
                throw new ArgumentNullException(nameof(todo));
            }

            _context.Todos.Remove(todo);
        }

        public void DeleteTodos(List<Todo> todoList)
        {
            foreach (Todo todo in todoList)
            {
                _context.Todos.Remove(todo);
            }
        }

        public IEnumerable<Todo> GetAllTodos()
        {
            IEnumerable<Todo> todos = _context.Todos.ToList();

            if(todos.Count() > 0)
            {
                return todos;
            }

            return null;
        }

        public int? GetNumberActiveTodos()
        {
            return _context.Todos.Where(t => t.Active == true).Count();
        }

        public int? GetNumberCompletedTodos()
        {
            return _context.Todos.Where(t => t.Active == false).Count();
        }

        public Todo GetTodoById(int Id)
        {
            return _context.Todos.FirstOrDefault(p => p.Id == Id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateTodo(Todo todo)
        {
        }
    }
}
