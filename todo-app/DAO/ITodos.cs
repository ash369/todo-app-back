﻿using System.Collections.Generic;
using todo_app.Models;

namespace todo_app.DAO
{
    public interface ITodos
    {
        bool SaveChanges();
        IEnumerable<Todo> GetAllTodos();
        Todo GetTodoById(int Id);
        int? GetNumberActiveTodos();
        int? GetNumberCompletedTodos();
        void CreateTodo(Todo todo);
        void UpdateTodo(Todo todo);
        void DeleteTodo (Todo todo);
        void DeleteTodos(List<Todo> todoList);
    }
}
