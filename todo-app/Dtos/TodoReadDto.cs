﻿namespace todo_app.Dtos
{
    public class TodoReadDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool Active { get; set; }
    }
}
