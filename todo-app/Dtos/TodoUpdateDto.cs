﻿using System.ComponentModel.DataAnnotations;

namespace todo_app.Dtos
{
    public class TodoUpdateDto
    {
        [Required]
        [MaxLength(250)]
        public string Title { get; set; }

        [Required]
        public bool Active { get; set; }
    }
}
