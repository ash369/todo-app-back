﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace todo_app.Dtos
{
    public class TodoCreateDto
    {
        public string Title { get; set; }

        public bool Active { get; set; }
    }
}
